## Code testing tasks :

1. Create log for every API activity (publish api / call api)
2. Create rest POST call to this API with Axios

```
      http://gvo37.mocklab.io/api/test
```

```
Body req :
  {
     "id": 12345,
     "value": "abc-def-ghi"
   }
```

3. Expose API with express that will have json response from the previous api call (No. 2)
4. Expose another API with express that will have a service to call this logical question below :

```
Write a program that should monitor a possibly infinite stream of characters input.
If it detects the sequence "aaa" it outputs a "0". If it detects the sequence "aba" it outputs a "1".
DO NOT detect sequences within sequences. For example:

The following sequence aababaaabaaa would produce the following result: 100
While the following sequence aaababaaaabbababa would produce the following result: 0101

API request will give the character sequence ex. ababaaababaaa. API response will produce the result ex. 101
```

5. Create API portal url for swagger API documentation related to No. 3 and 4 API
6. Deploy MySQL/MariaDB docker image as a docker container and create the database scheme using ORM to contain every request and response for point No. 3 and 4.
7. Make sure the application is able to connect and store the data to database container with ORM and promise.
8. Create unit testing.
9. Create dockerfile to dockerize this application.
10. Create parameterize value for the URL to be called by question No. 2 that can be inputted during docker run.
11. Make sure both application and database able to communicate while running as docker container.
12. Write clean and effective code for example : proper project structure, exception handling, parameterize config, etc.
