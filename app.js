const expressApp = require('./app/expressApp');
const router = require('./routes');

const base = new expressApp({
  router
});
