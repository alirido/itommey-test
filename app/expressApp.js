const express = require("express");
const bodyParser = require("body-parser");
const swaggerUi = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");

class expressApp {
  constructor(opts) {
    Object.assign(this, opts);

    this.app = express();

    this.app.use(bodyParser.json());

    if (this.router) {
      this.app.use("/api", this.router);
    }

    const options = {
      definition: {
        openapi: "3.0.0",
        info: {
          title: "ITOMMEY Express API with Swagger",
          version: "1.0.0",
          description:
            "This is API application made with Express and documented with Swagger",
          contact: {
            name: "Ali Rido",
            url: "",
            email: "mohalirido@email.com",
          },
        },
        servers: [
          {
            url: "http://localhost:3000",
          },
        ],
      },
      apis: ["./routes/index.js"],
    };

    const specs = swaggerJsDoc(options);
    this.app.use("/docs", swaggerUi.serve, swaggerUi.setup(specs));

    this.app.listen(3000, () => console.log("Listening on port 3000..."));
  }
}

module.exports = expressApp;
