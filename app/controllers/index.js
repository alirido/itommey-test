const axios = require("axios");

exports.restPost = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill id and value field",
    });
  }

  const { id, value } = req.body;

  axios({
    method: "post",
    url: "http://gvo37.mocklab.io/api/test",
    data: {
      id: id,
      value: value,
    },
  })
    .then((response) => {
      return res
        .status(200)
        .json({ message: "Successfully posting!", data: response.data });
    })
    .catch((err) => {
      return res
        .status(500)
        .json({ message: "Failed posting!", error: err.message });
    });
};

exports.sequenceProgram = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill sequence field",
    });
  }

  const { sequence } = req.body;
  let pos = 0;
  var ans = "";
  while (pos != -1) {
    let aaa = sequence.indexOf("aaa", pos);
    let aba = sequence.indexOf("aba", pos);

    if (aaa == -1) {
      if (aba == -1) {
        pos = -1;
      } else {
        ans += "1";
        pos = aba + 3;
      }
    } else if (aba == -1) {
      ans += "0";
      pos = aaa + 3;
    } else {
      if (aaa <= aba) {
        ans += "0";
        pos = aaa + 3;
      } else {
        ans += "1";
        pos = aba + 3;
      }
    }
  }

  return res.status(200).send(ans);
};
