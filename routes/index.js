const api = require("./api");

/**
 * @swagger
 * /api/rest:
 *   post:
 *     summary: Hit http://gvo37.mocklab.io/api/test
 *     tags: [rest]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               id:
 *                 type: integer
 *               value:
 *                 type: string
 *     responses:
 *       200:
 *         description: Successfully posting!
 *         content:
 *           application/json:
 *             schema:
 *               message: "Successfully posting!"
 *       500:
 *         description: Some server error
 */

/**
 * @swagger
 * /api/sequence-program:
 *   get:
 *     summary: Returns the converted sequence
 *     tags: [sequence-character]
 *     requestBody:
 *        description: Input sequence of characters
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: string
 *              properties:
 *                sequence:
 *                  type: string
 *                  example: "ababaaababaaa"
 *     responses:
 *       200:
 *         description: The result
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 *               example: 101
 */

module.exports = [api];
