const express = require("express");
const router = express.Router();
const controller = require("../../app/controllers");

router.post("/rest", controller.restPost);
router.get("/sequence-program", controller.sequenceProgram);

module.exports = router;
